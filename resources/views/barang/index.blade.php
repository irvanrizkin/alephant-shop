@extends('layouts.app')

@section('title', 'Alephant Shop')

@section('content')
<div class="container">
    @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
    @endif
    @if (session('failed'))
        <div class="alert alert-danger">
            {{ session('failed') }}
        </div>
    @endif
    <h1 class="mt-3">Daftar Barang</h1>
    @if (session('user_data'))
        @if (session('user_data')->email_user == 'admin@admin.com')
            <a href="{{ route('insert.barang.page') }}" class="btn btn-primary mb-3">Tambah Barang</a>
        @endif
    @endif
    <div class="row row-cols-1 row-cols-md-3">
        @forelse ($barangs as $barang)
            <div class="col mt-3">
                <a href="{{ route('barang.detail', ['id' => $barang->id_barang]) }}" class="index__anchor-none">
                    <div class="card">
                        <img src="{{ $barang->gambar_barang }}" class="card-img-top" alt="...">
                        <div class="card-body">
                            <h5 class="card-title"><b>{{ $barang->nama_barang }}</b></h5>
                            <p class="card-text">Stok : {{ $barang->stok_barang }}</p>
                            <p class="card-text">Harga : {{ $barang->harga_barang }}</p>
                            @if (session('user_data'))
                                <a href="{{ route('order.page', ['id' => $barang->id_barang]) }}" class="btn btn-primary mt-2">Beli</a>
                            @endif
                    </div>
                </div>
                </a>
            </div>
        @empty
            <div class="col-md-12 text-center">
                <p>Tidak ada barang</p>
            </div>
        @endforelse
    </div>
</div>
@endsection
