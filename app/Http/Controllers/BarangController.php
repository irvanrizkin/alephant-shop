<?php

namespace App\Http\Controllers;

use App\Models\Barang;
use Illuminate\Http\Request;

class BarangController extends Controller
{
    //
    public function insert_barang(Request $request)
    {
        Barang::create($request->all());

        return redirect()->route('index.barang')->with(['success' => 'Barang Berhasil Ditambahkan']);
    }

    public function barang_all()
    {
        $barangs = Barang::all();

        return view('barang.index')->with(['barangs' => $barangs]);
    }

    public function barang_id($id)
    {
        $barang = Barang::find($id);

        if (!$barang) {
            return redirect()->route('index.barang')->with(['failed' => 'Barang tidak ditemukan']);
        }

        return view('barang.detail')->with(['barang' => $barang]);
    }
}
