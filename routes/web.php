<?php

use App\Http\Controllers\BarangController;
use App\Http\Controllers\TransaksiController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [BarangController::class, 'barang_all'])->name('index.barang');
Route::get('/product/{id}', [BarangController::class, 'barang_id'])->name('barang.detail');

Route::middleware(['loggedin'])->group(function () {
    Route::get('/register', function () {
        return view('auth.register');
    })->name('register.page');

    Route::post('/register_action', [UserController::class, 'register'])->name('register');

    Route::get('/login', function () {
        return view('auth.login');
    })->name('login.page');

    Route::post('/login_action', [UserController::class, 'login'])->name('login');

});

Route::get('/logout_action', [UserController::class, 'logout'])->name('logout');

Route::middleware(['notadmin'])->group(function () {
    Route::get('/insert', function () {
        return view('barang.insert');
    })->name('insert.barang.page');

    Route::POST('/insert_action', [BarangController::class, 'insert_barang'])->name('insert.barang');
});

Route::middleware(['admin'])->group(function () {
    Route::get('/order/{id}', [TransaksiController::class, 'pesan_page'])->name('order.page');

    Route::post('/order_action', [TransaksiController::class, 'pesan'])->name('order.barang');
});
