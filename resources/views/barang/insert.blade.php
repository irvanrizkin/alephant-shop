@extends('layouts.app')

@section('title', 'Insert')

@section('content')
<div class="container">
    <h1 class="mt-3">Tambah Barang</h1>
    <form action="{{ route('insert.barang') }}" method="POST">
        @csrf
        <div class="mb-3">
            <label for="namaInput" class="form-label">Nama Barang</label>
            <input type="text" class="form-control" id="namaInput" name="nama_barang" required>
        </div>
        <div class="mb-3">
            <label for="stokBarang" class="form-label">Stok Barang</label>
            <input type="number" class="form-control" id="stokBarang" name="stok_barang" required>
        </div>
        <div class="mb-3">
            <label for="hargaBarang" class="form-label">Harga Barang</label>
            <input type="number" class="form-control" id="hargaBarang" name="harga_barang" required>
        </div>
        <div class="mb-5">
            <label for="gambarBarang" class="form-label">Gambar Barang</label>
            <input type="text" class="form-control" id="gambarBarang" name="gambar_barang" required>
        </div>
        <div class="mb-3 text-center">
            <img src="{{ asset('img/no-image.jpg') }}" class="img-thumbnail" id="gambarThumbnail">
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
</div>
@endsection

@section('custom-script')
<script>
    const gambarBarang = document.getElementById('gambarBarang');
    const gambarThumbnail = document.getElementById('gambarThumbnail');

    const noImageLink = gambarThumbnail.src;

    gambarBarang.addEventListener('focusout', () => {
        if (!gambarBarang.value) {
            gambarThumbnail.src = noImageLink;
            return;
        }
        gambarThumbnail.src = gambarBarang.value;
    });
</script>
@endsection
