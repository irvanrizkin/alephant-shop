<?php

namespace Database\Seeders;

use App\Models\Barang;
use Illuminate\Database\Seeder;

class BarangSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // row 1
        Barang::create([
            'nama_barang' => 'Melon Segar',
            'stok_barang' => 10,
            'harga_barang' => 40000,
            'gambar_barang' => 'https://akcdn.detik.net.id/api/wm/2019/02/26/90c536c7-8838-46a9-a8f5-8862c503f27e_169.jpeg',
        ]);
        Barang::create([
            'nama_barang' => 'Mangga Harum Manis',
            'stok_barang' => 15,
            'harga_barang' => 15000,
            'gambar_barang' => 'https://www.ubuy.co.id/productimg/?image=aHR0cHM6Ly9tLm1lZGlhLWFtYXpvbi5jb20vaW1hZ2VzL0kvOTFQRDNEUmJVYUwuX1NMMTUwMF8uanBn.jpg',
        ]);
        Barang::create([
            'nama_barang' => 'Jeruk Mandarin',
            'stok_barang' => 25,
            'harga_barang' => 7500,
            'gambar_barang' => 'https://s1.bukalapak.com/img/6847637162/large/Bibit_Benih_Seeds_Buah_Jeruk_Mandarin_Ponkam_Manis_Orange_Fr.jpg',
        ]);
        Barang::create([
            'nama_barang' => 'Lucky Strawberry',
            'stok_barang' => 10,
            'harga_barang' => 25000,
            'gambar_barang' => 'https://cdn-2.tstatic.net/manado/foto/bank/images/strawberry_20180316_204148.jpg',
        ]);
        Barang::create([
            'nama_barang' => 'Alpukat Alucard',
            'stok_barang' => 15,
            'harga_barang' => 30000,
            'gambar_barang' => 'https://cdn.idntimes.com/content-images/post/20210921/photo-1601039641847-7857b994d704-ebec26cd6a973a4213931b680c0a88ca.jpg',
        ]);
        // row 2
        Barang::create([
            'nama_barang' => 'Chicken Karage',
            'stok_barang' => 20,
            'harga_barang' => 40000,
            'gambar_barang' => 'https://laz-img-sg.alicdn.com/p/0b95bbdba8ee9f0c95757fef8a11a26a.jpg_720x720q80.jpg',
        ]);
        Barang::create([
            'nama_barang' => 'Chicken Nuggets',
            'stok_barang' => 5,
            'harga_barang' => 35000,
            'gambar_barang' => 'https://images.tokopedia.net/img/cache/500-square/VqbcmM/2021/6/6/aa137294-19cc-494b-a6ca-5666577db3fa.jpg',
        ]);
        Barang::create([
            'nama_barang' => 'Money Bag',
            'stok_barang' => 10,
            'harga_barang' => 45000,
            'gambar_barang' => 'https://www.static-src.com/wcsstore/Indraprastha/images/catalog/full//82/MTA-4002812/kraukk_kraukk_money_bag_250gr_15_pcs_full05.jpg',
        ]);
        Barang::create([
            'nama_barang' => 'Chicken Egg Rolls',
            'stok_barang' => 5,
            'harga_barang' => 55000,
            'gambar_barang' => 'https://m.media-amazon.com/images/I/81HPaQrJPCL._SL1500_.jpg',
        ]);
        Barang::create([
            'nama_barang' => 'Chicken Sausage',
            'stok_barang' => 10,
            'harga_barang' => 30000,
            'gambar_barang' => 'https://m.media-amazon.com/images/I/41uebSE22hL.jpg',
        ]);
        // row 3
        Barang::create([
            'nama_barang' => 'Mangkok Ayam',
            'stok_barang' => 10,
            'harga_barang' => 10000,
            'gambar_barang' => 'http://www.mastergrosir.com/145-large_default/mangkok-ayam-6-inchi-cap-ayam-.jpg',
        ]);
        Barang::create([
            'nama_barang' => 'Piring Melamin',
            'stok_barang' => 5,
            'harga_barang' => 15000,
            'gambar_barang' => 'https://id-live-05.slatic.net/p/14b62aa64527b659eafd43ab751d7b0e.jpg_720x720q80.jpg',
        ]);
        Barang::create([
            'nama_barang' => 'Sendok Garpu Bundle',
            'stok_barang' => 20,
            'harga_barang' => 10000,
            'gambar_barang' => 'https://images.duniamasak.com/images/12841/sendok-garpu-maspion-aprilia-set-12pcs-24447_521.png',
        ]);
        Barang::create([
            'nama_barang' => 'Sendok Es Krim',
            'stok_barang' => 15,
            'harga_barang' => 15000,
            'gambar_barang' => 'https://id-live-05.slatic.net/p/01de46cc1f9302c250e920c8bb4ce839.jpg_720x720q80.jpg',
        ]);
        Barang::create([
            'nama_barang' => 'Chop Stick',
            'stok_barang' => 8,
            'harga_barang' => 12000,
            'gambar_barang' => 'https://electropapa.com/media/catalog/product/cache/1/image/800x800/9df78eab33525d08d6e5fb8d27136e95/j/u/juni-2017-essstaebchen_paar_ow2-k-41_1ebay.jpg',
        ]);          
    }
}
