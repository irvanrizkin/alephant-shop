<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>@yield('title')</title>
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body class="vh-100">
    @include('layouts.nav')
    @yield('content')
    @include('layouts.footer')

    <script src="{{ asset('js/app.js') }}" defer></script>
    @yield('custom-script')
</body>
</html>
