# Alephant Shop
Implementation project from Information System Implementation and Evaluation (IESI) course

## Installation
Install packages listed in composer.json
```
composer install
```
Make a copy or rename .env.example file to .env
```
cp .env.example .env
```
Generate the laravel project key
```
php artisan key:generate
```
Change database name, username, and password at .env
```
DB_DATABASE=alephant
DB_USERNAME=root
DB_PASSWORD=
```
Migrate the table and seed the dummy
```
php artisan migrate:fresh --seed
```
Install package listed in package.json
```
npm install
```
Compile CSS and JS into one file
```
npm run dev
```
Start the server
```
php artisan serve
```
