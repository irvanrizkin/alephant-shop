<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transaksi extends Model
{
    use HasFactory;

    protected $fillable = [
        'id_barang',
        'id_user',
        'jumlah_barang',
        'total_tagihan',
        'status_pembayaran',
        'status_pengiriman',
        'nomor_resi'
    ];

    protected $primaryKey = 'id_transaksi';

    public function barang()
    {
        return $this->belongsTo(Barang::class, 'id_barang');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'id_user');
    }
}
