<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class RedirectIfAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if ($request->session()->get('user_data')->email_user == 'admin@admin.com') {
            return redirect()->route('index.barang')->with(['failed' => 'Admin tidak dapat membeli barang']);
        }
        return $next($request);
    }
}
