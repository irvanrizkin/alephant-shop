@extends('layouts.app')

@section('title', 'Register')

@section('content')
<div class="container">
    <h1 class="my-3">Pemesanan</h1>
    <table class="table">
        <tbody>
            <tr>
                <td>Pembeli</td>
                <td>{{ session('user_data')->name_user }}</td>
            </tr>
            <tr>
                <td>Nama Barang</td>
                <td>{{ $barang->nama_barang }}</td>
            </tr>
            <tr>
                <td>Harga</td>
                <td>{{ $barang->harga_barang }}</td>
            </tr>
        </tbody>
    </table>

    <form action="{{ route('order.barang') }}" method="POST">
        @csrf
        {{ session(['id_barang' => $barang->id_barang]) }}
        <div class="mb-3">
            <label for="qtyInput" class="form-label">Qty</label>
            <input type="number" class="form-control" id="qtyInput" name="jumlah_barang" required>
        </div>
        <div class="mb-3">
            <label for="pembayaranInput" class="form-label">Nomor Pembayaran</label>
            <input type="text" class="form-control" id="pembayaranInput" name="nomor_pembayaran">
            <small class="form-text text-muted">Kosongi jika membayar nanti</small>
        </div>
        <button type="submit" class="btn btn-primary">Bayar</button>
    </form>
</div>
@endsection
