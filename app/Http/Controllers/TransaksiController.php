<?php

namespace App\Http\Controllers;

use App\Models\Barang;
use App\Models\Transaksi;
use Illuminate\Http\Request;

class TransaksiController extends Controller
{
    //
    public function pesan_page($id)
    {
        $barang = Barang::find($id);
        if ($barang->stok_barang == 0) {
            return redirect()->route('index.barang')->with(['failed' => 'Barang telah habis']);
        }
        return view('transaksi.order')->with(['barang' => $barang]);
    }

    public function pesan(Request $request)
    {
        if ($request->session()->missing('id_barang')) {
            return redirect()->route('index.barang')->with(['failed' => 'ID Barang belum tersimpan']);
        }

        $barang = Barang::find($request->session()->get('id_barang'));
        $barang->update([
            'stok_barang' => $barang->stok_barang - $request->jumlah_barang,
        ]);
        $harga = $barang->harga_barang * $request->jumlah_barang;

        Transaksi::create([
            'id_barang' => $request->session()->pull('id_barang'),
            'id_user' => $request->session()->get('user_data')->id_user,
            'jumlah_barang' => $request->jumlah_barang,
            'total_tagihan' => $harga,
            'status_pembayaran' => $request->nomor_pembayaran ?? 'Belum',
            'status_pengiriman' => 'Belum',
            'nomor_resi' => 'Belum',
        ]);

        return redirect()->route('index.barang')->with(['success' => 'Pemesanan berhasil']);
    }


}
