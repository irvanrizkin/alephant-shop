<nav class="navbar navbar-light bg-light">
  <div class="container-fluid">
    <a href="{{ route('index.barang') }}" class="navbar-brand">Alephant Shop</a>
    @if (session('user_data'))
        <div class="d-flex">
            <span class="navbar-text mr-3">
            {{ session('user_data')->email_user }}
            </span>
        <a href="{{ route('logout') }}" class="btn btn-outline-success" type="submit">Logout</a>
        </div>
    @else
        <div class="d-flex">
        <a href="{{ route('login.page') }}" class="btn btn-outline-success" type="submit">Login</a>
        </div>
    @endif

  </div>
</nav>
