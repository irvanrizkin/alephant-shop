@extends('layouts.app')

@section('title', 'Register')

@section('content')
<div class="container">
    @if (session('failed'))
        <div class="alert alert-danger">
            {{ session('failed') }}
        </div>
    @endif
    <h1 class="mt-3">Register</h1>
    <form action="{{ route('register') }}" method="POST">
        @csrf
        <div class="mb-3">
            <label for="nameInput" class="form-label">Name</label>
            <input type="text" class="form-control" id="nameInput" name="name_user" required>
        </div>
        <div class="mb-3">
            <label for="emailInput" class="form-label">Email</label>
            <input type="email" class="form-control" id="emailInput" name="email_user" required>
        </div>
        <div class="mb-3">
            <label for="passwordInput" class="form-label">Password</label>
            <input type="password" class="form-control" id="passwordInput" name="password_user" required>
        </div>
        <div class="mb-3">
            <label for="alamatInput" class="form-label">Alamat</label>
            <textarea class="form-control" id="alamatInput" rows="3" name="alamat_user" required></textarea>
        </div>
        <div class="mb-3">
            <label for="notelpInput" class="form-label">Nomor Telepon</label>
            <input type="text" class="form-control" id="notelpInput" name="no_telp_user" required>
        </div>
        <div class="mb-3">
        <p class="form-text text-muted">Sudah memiliki akun? <a href="{{ route('login.page') }}">Masuk Sekarang</a></p>
        </div>
        <button type="submit" class="btn btn-primary">Daftar</button>
    </form>
</div>
@endsection
