@extends('layouts.app')

@section('title', $barang->nama_barang)

@section('content')
<div class="container">
    <h1 class="my-3">{{ $barang->nama_barang }}</h1>
    <div class="row">
        <div class="col-md-6 col-sm-12">
            <img src="{{ $barang->gambar_barang }}" alt="" class="img-fluid">
        </div>
        <div class="col-md-6 col-sm-12 d-flex justify-content-center flex-column">
            <h2>Stok</h2>
            <h4>{{ $barang->stok_barang }}</h4>
            <h2>Harga</h2>
            <h4>Rp.{{ $barang->harga_barang }}</h4>
            @if (session('user_data'))
                <a href="{{ route('order.page', ['id' => $barang->id_barang]) }}" class="btn btn-primary mt-2">Beli</a>
            @endif
        </div>
    </div>
    @if (session('user_data'))
        @if (session('user_data')->email_user == 'admin@admin.com')
        <h1 class="my-3">Transaksi</h1>
            @if ($barang->transaksi)
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">Pembeli</th>
                            <th scope="col">Qty</th>
                            <th scope="col">Tagihan</th>
                            <th scope="col">Status Pembayaran</th>
                            <th scope="col">Status Pengiriman</th>
                            <th scope="col">Resi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($barang->transaksi as $transaksi)
                            <tr>
                                <td>{{ $transaksi->user->name_user }}</td>
                                <td>{{ $transaksi->jumlah_barang }}</td>
                                <td>{{ $transaksi->total_tagihan }}</td>
                                <td>{{ $transaksi->status_pembayaran }}</td>
                                <td>{{ $transaksi->status_pengiriman }}</td>
                                <td>{{ $transaksi->nomor_resi }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            @endif
        @endif
    @endif
</div>
@endsection
