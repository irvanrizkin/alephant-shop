<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        User::create([
            'name_user' => 'Admin Alephant',
            'email_user' => 'admin@admin.com',
            'password_user' => 'admin',
            'alamat_user' => 'Malang',
            'no_telp_user' => '085212341234',
        ]);
    }
}
