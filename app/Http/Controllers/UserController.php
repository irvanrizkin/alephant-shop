<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    //
    public function register(Request $request)
    {
        $user = User::where('email_user', $request->email_user)->first();
        if ($user) {
            return redirect()->route('register.page')->with(['failed' => 'Email telah digunakan']);
        }
        User::create($request->all());
        return redirect()->route('login.page')->with(['success' => 'Registrasi Berhasil']);
    }

    public function login(Request $request)
    {
        $user = User::where('email_user', $request->email_user)->first();
        if (!$user) {
            return redirect()->route('login.page')->with(['failed' => 'Email atau password tidak sesuai']);
        }
        if ($user->password_user != $request->password_user) {
            return redirect()->route('login.page')->with(['failed' => 'Email atau password tidak sesuai']);
        }
        $request->session()->put('user_data', $user);
        return redirect()->route('index.barang');
    }

    public function logout(Request $request)
    {
        $request->session()->flush();
        return redirect()->route('login.page');
    }
}
