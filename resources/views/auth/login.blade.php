@extends('layouts.app')

@section('title', 'Login')

@section('content')
<div class="container">
    @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
    @endif
    @if (session('failed'))
        <div class="alert alert-danger">
            {{ session('failed') }}
        </div>
    @endif
    <h1 class="mt-3">Login</h1>
    <form action="{{ route('login') }}" method="POST">
        @csrf
        <div class="mb-3">
            <label for="emailInput" class="form-label">Email</label>
            <input type="email" class="form-control" id="emailInput" name="email_user" required>
        </div>
        <div class="mb-3">
            <label for="passwordInput" class="form-label">Password</label>
            <input type="password" class="form-control" id="passwordInput" name="password_user" required>
        </div>
        <p class="form-text text-muted">Belum memiliki akun? <a href="{{ route('register.page') }}">Daftar Sekarang</a></p>
        <button type="submit" class="btn btn-primary">Masuk</button>
    </form>
</div>
@endsection
